package stream;

import java.io.IOException;
import java.rmi.UnexpectedException;

public class Main {
    public static void main(String[] args) {
        ProductRepository productRepository = new ProductsRepositoryFileBasedImpl("C:\\Users\\Андрей Паньковский\\Desktop\\Inn\\AW\\AW\\src\\stream\\input.txt");
        try {
            System.out.println(productRepository.findById(1));
            System.out.println("============");
            System.out.println(productRepository.findById(3));
            System.out.println("============");
            System.out.println(productRepository.findById(7));
            System.out.println("============");
            System.out.println(productRepository.findAllByTitleLike("ол"));
            System.out.println("============");
            System.out.println(productRepository.findAllByTitleLike("оло"));
        } catch (IOException ex) {
            System.out.println("Error!");
        }

    }
}
