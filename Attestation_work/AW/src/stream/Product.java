package stream;

public record Product(Integer id, String name, Double cost, int count) {
}
