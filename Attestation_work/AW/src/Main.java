package hashMap;

import java.util.HashMap;

import java.util.Map;

public class Main {
    public static void main(String[] args) {
        String[] strings = "Hello! Hello! Hello!".split(" "); //есть массив стрингов; split делит массив строк
        Map<String, Integer> map = new HashMap<>(); //создали ХэшМап

        for (String str : strings) { //пробегаемся по ХМ и по ключу достаем значение,
            int count = map.getOrDefault(str, 0) + 1; //если значения нет 0, иначе берем слово и когда его встречаем +1
            map.put(str, count); //и сразу кладем в ХМ
        }

        Map.Entry<String, Integer> maxEntry = null;//внутри МАПы, ключ значение
        for (Map.Entry<String, Integer> entry : map.entrySet()) { //потом снова пробегаемся и достаем пары ключей значений
            if (maxEntry == null || entry.getValue() > maxEntry.getValue()) {//ищем значение с макс велью
                maxEntry = entry; //и потом присваиваем
            }
        }


    }
}