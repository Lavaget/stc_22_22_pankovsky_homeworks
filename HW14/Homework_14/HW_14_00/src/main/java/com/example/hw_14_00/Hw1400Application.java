package com.example.hw_14_00;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hw1400Application {

	public static void main(String[] args) {
		SpringApplication.run(Hw1400Application.class, args);
	}

}
