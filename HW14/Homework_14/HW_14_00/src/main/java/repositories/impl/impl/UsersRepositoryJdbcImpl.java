package repositories.impl.impl;

import com.example.hw_14_00.User;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import repositories.impl.UsersRepository;

import java.util.List;

@RequiredArgsConstructor
@Repository

public class UsersRepositoryJdbcImpl implements UsersRepository {

    private final JdbcTemplate JdbcTemplate;

    private static final String SQL_SELECT_ALL = "select * from account";

    private final static RowMapper<User> usersRowMapper = (row, rowNum) ->
            User.builder()
                            .id(row.getLong("id"))
                                    .email(row.getString("email"))
                                            .password(row.getString("password"))
                                                    .build();
    public List<User> findAll()

}
