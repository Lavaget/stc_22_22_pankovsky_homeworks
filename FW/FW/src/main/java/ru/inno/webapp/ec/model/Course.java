package ru.inno.webapp.ec.model;

import javax.persistence.*;
import lombok.*;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import java.time.LocalDate;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"lessons", "students"})
@Entity
@DynamicInsert
public class Course {
    public enum Status {
        NOT_CONFIRMED, DELETED
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @Column(length = 1000)
    private String description;

    @Column(nullable = false,columnDefinition = "date default 'epoch'")
    private LocalDate start;
    @Column(nullable = false,columnDefinition = "date default 'epoch'")
    private LocalDate finish;

    @OneToMany(mappedBy = "course", fetch = FetchType.EAGER)
    private Set<Lesson> lessons;

    @ManyToMany(mappedBy = "courses", fetch = FetchType.EAGER)
    private Set<User> students;

    @Enumerated(value = EnumType.STRING)
    private Status status;
}
