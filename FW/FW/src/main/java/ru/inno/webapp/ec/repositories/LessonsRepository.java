package ru.inno.webapp.ec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.webapp.ec.model.Course;
import ru.inno.webapp.ec.model.Lesson;

import java.util.List;

public interface LessonsRepository extends JpaRepository<Lesson,Long> {
    List<Lesson>findAllByStatusNot(Lesson.Status state);

    List<Lesson> findAllByCourseNull();

    List<Lesson> findAllByCourse(Course course);
}
