package ru.inno.webapp.ec.service;

import ru.inno.webapp.ec.dto.CoursesForm;
import ru.inno.webapp.ec.model.Course;
import ru.inno.webapp.ec.model.Lesson;
import ru.inno.webapp.ec.model.User;

import java.util.List;

public interface CoursesService {
    List<Course>getAllCourses();
    void addStudentToCourse(Long courseId, Long studentId);

    Course getCourse(Long courseId);

    List<User> getNotInCourseStudents(Long courseId);

    List<User> getInCourseStudents(Long courseId);

    void deleteCourse(Long courseId);

    void addCourse(CoursesForm course);

    void updateCourse(Long courseId, CoursesForm course);
    void addLessonToCourse(Long courseId, Long lessonId);
    List<Lesson> getNotInCourseLessons();
    List<Lesson> getInCourseLessons(Long courseId);
}
