public class Main {

    public static void main(String[] args) {
        assert calcSumOfSpan(10, 9) == -1;
        assert calcSumOfSpan(10, 10) == 10;
        assert calcSumOfSpan(10, 15) == 75;

    }


    private static int calcSumOfSpan(int from, int to) {

        if (from > to) {
            return -1;

        }

        int sum = 0;

        for (int i = from; i <= to; i++) {

            sum += i;
        }
        return sum;
    }

}



