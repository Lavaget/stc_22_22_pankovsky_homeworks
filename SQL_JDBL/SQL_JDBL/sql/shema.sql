drop table if exists student;

create table student (
                         id bigserial primary key,
                         email char(20) unique,
                         password char(100),
                         first_name char(20) default 'DEFAULT_FIRSTNAME',
                         last_name char(20) default 'DEFAULT_LASTTNAME',
                         age integer check (age >= 0 and age <= 120) not null,
                         is_worer bool
);

alter table student alter column email set not null;

insert into student (email, password, age, is_worer)
values ('marsel@gmail.com', 'qwerty007', 28, true);
insert into student (email, password, age, is_worer)
values ('stanislav@gmail.ru', 'qwerty008', 38, false);
insert into student (email, password, age, is_worer)
values ('yurii@yandex.ru', 'qwerty009', 27, true);
insert into student (email, password, age, is_worer)
values ('vladislav@.yahoo.com', 'qwerty010', 20, false);

update student
set first_name = 'Марсель', last_name = 'Сидиков'
where id = 1;
update student
set first_name = 'Станислав', last_name = 'Джербеков'
where id = 2;
update student
set first_name = 'Юрий', last_name = 'Вертелецкий'
where id = 3;
update student
set first_name = 'Владислав', last_name = 'Малышев'
where id = 4;

alter table student
    add column average double precision check ( average >=0 and average <= 5) default 0;


